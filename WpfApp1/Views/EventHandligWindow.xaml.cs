﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfApp1.Views
{
    /// <summary>
    /// Interaction logic for EventHandligWindow.xaml
    /// </summary>
    public partial class EventHandligWindow : Window
    {
        public EventHandligWindow()
        {
            InitializeComponent();
        }

        public void MoneyTexCh(object sender, RoutedEventArgs e)
        {
            decimal value = 0;
            double dvalue = 0;
            var t = (TextBox)sender;
            var val = this.txtFeeAmount.Text;

            decimal.TryParse(val, out value);
            double.TryParse(val, out dvalue);

            int count = BitConverter.GetBytes(decimal.GetBits(value)[3])[2];

            if (t.SelectedText == t.Text)
            {

            }
            else
            {
                if (count == 2)
                {
                    e.Handled = true;
                }

            }
        }

    }
}
