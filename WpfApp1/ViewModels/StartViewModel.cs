﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WpfApp1.Views;

namespace WpfApp1.ViewModels
{
    public class StartViewModel : ObservableObject
    {
        public ICommand WordReverseCommand
        {
            get { return new DelegateCommand(WordReverse); }
        }

        public ICommand StackCommand
        {
            get { return new DelegateCommand(Stack); }
        }

        public ICommand MatchCommand
        {
            get { return new DelegateCommand(Match); }
        }

        public ICommand EventHandligCommand
        {
            get { return new DelegateCommand(EventHandlig); }
        }

        


        public ICommand CancelCommand
        {
            get { return new DelegateCommand(CloseWindow); }
        }

        private void WordReverse()
        {
            WordReversal window = new WordReversal() { DataContext = new WordReversalViewModel() };
            window.Show();
        }

        private void Stack()
        {
            StackWindow window = new StackWindow() { DataContext = new StackViewModel() };
            window.Show();
        }

        private void Match()
        {
            MatchCharactersWindow window = new MatchCharactersWindow() { DataContext = new MatchCharactersModel() };
            window.Show();
        }

        private void EventHandlig()
        {
            EventHandligWindow window = new EventHandligWindow() { DataContext = new EventHandligModel() };
            window.Show();
        }

        

        private void CloseWindow()
        {
            App.Current.MainWindow.Close();
        }


    }
}
