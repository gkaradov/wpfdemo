﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace WpfApp1.ViewModels
{
    public class EventHandligModel : ObservableObject
    {
        private decimal _feeAmount;

        public decimal FeeAmount
        {
            get { return this._feeAmount; }
            set
            {
                this._feeAmount = value;

                this.OnPropertyChanged("FeeAmount");
            }
        }
    }
}
