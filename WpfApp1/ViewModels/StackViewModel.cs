﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Text;
using System.ComponentModel;

namespace WpfApp1.ViewModels
{

    public class StackViewModel : ObservableObject
    {

        private List<int> _myStack;
        private string _inputText;
        private string _sizeText;
        private string _isEmptyText;
        private string _stackText;


        public StackViewModel()
        {
            _myStack = new List<int>();

        }

        public string InputText
        {
            get { return _inputText; }
            set
            {
                _inputText = value;
                this.OnPropertyChanged("InputText");
            }
        }

        public string SizeText
        {
            get { return _sizeText; }
            set
            {
                _sizeText = value;
                this.OnPropertyChanged("SizeText");
            }
        }

        public string IsEmptyText
        {
            get { return _isEmptyText; }
            set
            {
                _isEmptyText = value;
                this.OnPropertyChanged("IsEmptyText");
            }
        }


        public string StackText
        {
            get { return _stackText; }
            set
            {
                _stackText = value;
                this.OnPropertyChanged("StackText");
            }
        }

        public ICommand PushCommand
        {
            get { return new DelegateCommand(Push); }
        }

        public ICommand PopCommand
        {
            get { return new DelegateCommand(Pop); }
        }

        public ICommand SizeCommand
        {
            get { return new DelegateCommand(Size); }
        }

        public ICommand IsEmptyCommand
        {
            get { return new DelegateCommand(IsEmpty); }
        }

        private void Push()
        {
            if (_inputText != string.Empty)
            {
                int newElement;

                if (int.TryParse(_inputText, out newElement))
                {

                    _myStack.Add(newElement);
                    OutputStack();
                    
                    SizeText = _myStack.Count.ToString();
                    IsEmptyText = "False";
                }
            }

        }

        private void Pop()
        {
            if(_myStack.Count > 0)
            {
                _myStack.RemoveAt(_myStack.Count - 1);
                OutputStack();
                if (_myStack.Count == 0)
                {
                    IsEmptyText = "True";
                }
                else
                {
                    IsEmptyText = "True";
                }

            }
        }

        private void OutputStack()
        {
            var outText = new StringBuilder();
            var localStack = _myStack.ToArray();
         
            for(var i = localStack.Length -1; i >= 0; i--)
            {
                outText.Append(localStack[i].ToString());
                outText.AppendLine();
            }

            StackText = outText.ToString();
        }

        private void Size()
        {
            SizeText = _myStack.Count.ToString();
        }

        private void IsEmpty()
        {
            if (_myStack.Count == 0)
            {
                IsEmptyText = "True";
            }
            else
            {
                IsEmptyText = "True";
            }
        }

    }
}
