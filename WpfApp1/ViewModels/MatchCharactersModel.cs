﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Linq;
using System.Text;
using System.ComponentModel;

namespace WpfApp1.ViewModels
{
    public class MatchCharactersModel : ObservableObject
    {
        private string _inputOneText;
        private string _inputTwoText;
        private string _outputText;

        public string InputOneText
        {
            get { return _inputOneText; }
            set
            {
                _inputOneText = value;
                this.OnPropertyChanged("InputOneText");
            }
        }

        public string InputTwoText
        {
            get { return _inputTwoText; }
            set
            {
                _inputTwoText = value;
                this.OnPropertyChanged("InputTwoText");
            }
        }

        public string OutputText
        {
            get { return _outputText; }
            set
            {
                _outputText = value;
                this.OnPropertyChanged("OutputText");
            }
        }

        public ICommand MatchCommand
        {
            get { return new DelegateCommand(Match); }
        }

        public ICommand MatchLINQCommand
        {
            get { return new DelegateCommand(MatchLINQ); }
        }


        private void Match()
        {
            // Order(N) using bool aray with index of the ASCII charcter values - will only work on ASCII and UDF08
            if (_inputOneText != string.Empty && _inputTwoText != string.Empty)
            {

                var inStrOne = this._inputOneText.ToCharArray();
                var inStrTwo = this._inputTwoText.ToCharArray();
                var outStr = string.Empty;

                bool[] flags = new bool[256];  //sizeOf(char)=256

                for (int i = 0; i < inStrTwo.Length; i++)
                    flags[inStrTwo[i]] = true;

                for (int j = 0; j < inStrOne.Length; j++)
                    if (flags[inStrOne[j]] == true)
                    {
                        outStr += inStrOne[j];  // order of a
                        flags[inStrOne[j]] = false;
                    }

                OutputText = outStr.ToString();
            }
        }

        private void MatchLINQ()
        {

            // Simple implementation using LINQ commands - may suffer in performance on large sets. Not sure of the Order
            if (_inputOneText != string.Empty && _inputTwoText != string.Empty)
            {

                var inStrOne = this._inputOneText;
                var inStrTwo = this._inputTwoText;
                var outStr =  new string((inStrOne.Intersect(inStrTwo)).ToArray());
                OutputText = outStr;
            }


        }


    }
}
