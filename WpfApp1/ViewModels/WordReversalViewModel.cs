﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Text;
using System.ComponentModel;

namespace WpfApp1.ViewModels
{

    public class WordReversalViewModel : ObservableObject
    {

        private string _inputText;
        private string _outputText;

        public string InputText
        {
            get { return _inputText; }
            set
            {
                _inputText = value;
                this.OnPropertyChanged("InputText");
            }
        }

        public string OutputText
        {
            get { return _outputText; }
            set
            {
                _outputText = value;
                this.OnPropertyChanged("OutputText");
            }
        }

        public ICommand ReverseCommand
        {
            get { return new DelegateCommand(Reverse); }
        }

        public ICommand CancelCommand
        {
            get { return new DelegateCommand(CloseWindow); }
        }

        private void Reverse()
        {

            if(_inputText != string.Empty)
            {

                var inStr = new StringBuilder(this.InputText);

                for (int i = 0; i <= inStr.Length/2; i++)
                {
                    var temp = inStr[i];
                    inStr[i] = inStr[inStr.Length - i-1];
                    inStr[inStr.Length - i-1] = temp;
                }

                OutputText = inStr.ToString();
            }


        }

        private void CloseWindow()
        {
            App.Current.MainWindow.Close();
        }

    }
}
